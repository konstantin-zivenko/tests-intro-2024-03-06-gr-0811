# tests_intro_2023_11_27_gr1008

---

[video](https://youtu.be/ALBzLgjNEyE)


Task:

Write an application that reverses all the words of input text:
- Example "abcd efgh" => "dcba hgfe"
- All non-letter symbols should stay on the same places:
- Example "a1bcd efg!h" => "d1cba hgf!e"
- Use Latin alphabet for test only.

You should write a function which returns reverse text