import pytest

from tricky_revers import tricky_revers


@pytest.mark.parametrize(
    "argument, result",
    (
            ("", ""),
            ("a", "a"),
            ("ab", "ba"),
            ("abcd efgh", "dcba hgfe"),
            ("a1bcd efg!h", "d1cba hgf!e"),
    )
)
def test_typical_case(argument, result):
    assert tricky_revers(argument) == result


atypical_cases = (None, True, [], (1, 2, 3), 31, 0.009)


@pytest.mark.parametrize("argument", atypical_cases)
def test_atypical_case(argument):
    with pytest.raises(TypeError) as err:
        tricky_revers(argument)
        print(err)


if __name__ == '__main__':
    pytest.main()
