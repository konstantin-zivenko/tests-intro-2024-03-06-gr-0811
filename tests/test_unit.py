import unittest
from parameterized import parameterized
from tricky_revers import tricky_revers


class TestTrickyRevers(unittest.TestCase):
    @parameterized.expand(
        (
                ("", ""),
                ("a", "a"),
                ("ab", "ba"),
                ("abcd efgh", "dcba hgfe"),
                ("a1bcd efg!h", "d1cba hgf!e"),
        )
    )
    def test_typical(self, argument, result):
        self.assertEqual(tricky_revers(argument), result)

    @parameterized.expand(
        ((None,), (12,))
    )
    def test_atypical(self, argument):
        self.assertRaises(TypeError, tricky_revers, argument)


if __name__ == "__main__":
    unittest.main()
