def get_tricky_reversed_words(word: str) -> str:
    inverted_word = ""
    letters = [letter for letter in word if letter.isalpha()]
    for symbol in word:
        if symbol.isalpha():
            inverted_word += letters.pop()
        else:
            inverted_word += symbol
    return inverted_word


def tricky_revers(text: str) -> str:
    if not isinstance(text, str):
        raise TypeError(f"unsupported type: {type(text)}. Expected 'str'.")
    words = text.split()
    reversed_words = map(get_tricky_reversed_words, words)
    return " ".join(reversed_words)


if __name__ == "__main__":
    cases = (
        ("", ""),
        ("a", "a"),
        ("ab", "ba"),
        ("abcd efgh", "dcba hgfe"),
        ("a1bcd efg!h", "d1cba hgf!e"),
    )
    for arg, result in cases:
        assert tricky_revers(
            arg) == result, f"tricky_revers({arg!r}) --> {tricky_revers(arg)}, but expected: {result!r}"
